#!/bin/sh
# IPBlock
# creates two ipset in-memory databases for subnets and hosts to block using iptables
# downloads blocklists & adds ips to ipset dbs.
# installs iptables reject rules for each ipset db if needed
# installs crontab job if needed
#
# hosts files has one ip per line as the first word
# network files have one line per network with no subnet prefix, /24 is assumed.
# add additional file imports by declaring array entry N+1
#
# these iptables rules are installed if required
# iptables -I INPUT 1 -m set --match-set blocklist_hostblocks src -j REJECT
# iptables -I INPUT 1 -m set --match-set blocklist_networkblocks src -j REJECT
# 
# can be installed using (as root): 
# wget -O /usr/src/ipblock.sh https://bitbucket.org/sp00kz/public/raw/master/ipblock.sh && chmod +x /usr/src/ipblock.sh && /usr/src/ipblock.sh
# update using (as root):
# wget -O /usr/src/ipblock.sh https://bitbucket.org/sp00kz/public/raw/master/ipblock.sh && chmod +x /usr/src/ipblock.sh




setHosts="blocklist_hostblocks"
setNetworks="blocklist_networkblocks"

# list of files containing network blocks to list
target_networks[0]="https://feeds.dshield.org/block.txt"

# list of files containing network hosts to block
target_hosts[0]="https://lists.blocklist.de/lists/all.txt"
target_hosts[1]="https://bitbucket.org/sp00kz/public/raw/master/blacklist"

tmpFile=/tmp/.ipset-tmp
rm -f $tmpFile
# download & append network blocklist
for currentFile in "${target_networks[@]}"
do
    wget -qO - $currentFile >> $tmpFile
done
opt="hash:net hashsize 64"
tmp=${setNetworks}-tmp
# create temp ipset list
/usr/sbin/ipset create $tmp $opt
# add networks to iplist
networks="$(grep -E '^[0-9]' $tmpFile | sed -rne 's/(^([0-9]{1,3}\.){3}[0-9]{1,3}).*$/\1/p')"
for i in $networks; do
    /usr/sbin/ipset -! add $tmp ${i}/24
done
# create destination if doesn't exist
/usr/sbin/ipset create -exist $setNetworks $opt &&
# swap temp to dest and delete temp
/usr/sbin/ipset swap $tmp $setNetworks &&
/usr/sbin/ipset destroy $tmp &&
echo "IPSet: $setNetworks updated"
rm -f $tmpFile


# update host blocklist
for currentFile in "${target_hosts[@]}"
do
    wget -qO - $currentFile >> $tmpFile
done
opt="hash:ip hashsize 102400"
tmp=${setHosts}-tmp
# create temp ipset list
/usr/sbin/ipset create $tmp $opt
# add networks to iplist
networks="$(grep -E '^[0-9]' $tmpFile | sed -rne 's/(^([0-9]{1,3}\.){3}[0-9]{1,3}).*$/\1/p')"
for i in $networks; do
    /usr/sbin/ipset -! add $tmp ${i}
done
# create destination if doesn't exist
/usr/sbin/ipset create -exist $setHosts $opt &&
# swap temp to dest and delete temp
/usr/sbin/ipset swap $tmp $setHosts &&
/usr/sbin/ipset destroy $tmp &&
echo "IPSet: $setHosts updated"
rm -f $tmpFile


# update iptables to block hosts if it needs
if /sbin/iptables -L -n | grep -q -- "$setHosts";
 then
  :
  # echo "firewall already configured for $setHosts."
 else
  echo "configuring firewall for $setHosts."
  /sbin/iptables -I INPUT 1 -m set --match-set $setHosts src -j REJECT
fi 
if /sbin/iptables -L -n | grep -q -- "$setNetworks";
 then
  :
  # echo "firewall already configured for $setNetworks."
 else
   echo "configuring firewall for $setNetworks."
  /sbin/iptables -I INPUT 1 -m set --match-set $setNetworks src -j REJECT
fi 
# check and install on crontab
croncmd="/usr/src/ipblock.sh >> /var/log/ipblock.log 2>&1"
cronjob="0 * * * * $croncmd"
( crontab -l | grep -v -F "$croncmd" ; echo "$cronjob" ) | crontab -
