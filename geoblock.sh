#!/bin/sh
# IP-Restrict
# Restricts inbound network connections to whitelisted subnets
# downloads subnet lists from ipdeny
#
#
# can be installed using (as root): 
# wget -O /usr/src/geoblock.sh https://bitbucket.org/sp00kz/public/raw/master/geoblock.sh && chmod +x /usr/src/geoblock.sh && /usr/src/geoblock.sh
# update using (as root):
# wget -O /usr/src/geoblock.sh https://bitbucket.org/sp00kz/public/raw/master/geoblock.sh && chmod +x /usr/src/geoblock.sh

setNetworks="geoip_allow"
tmpFile=/tmp/.geoip-ipset-tmp
rm -f $tmpFile

# list of files containing network blocks to allow
target_networks[0]="http://www.ipdeny.com/ipblocks/data/aggregated/us-aggregated.zone"
target_networks[1]="http://www.ipdeny.com/ipblocks/data/aggregated/ca-aggregated.zone"


# download & append network blocklist
for currentFile in "${target_networks[@]}"
do
    wget -qO - $currentFile >> $tmpFile
done
echo "10.0.0.0/8" >> $tmpFile
echo "127.0.0.0/8" >> $tmpFile

opt="hash:net"
tmp=${setNetworks}-tmp

# create temp ipset list
/usr/sbin/ipset create $tmp $opt

# add networks to iplist
networks="$(grep -E '^[0-9]' $tmpFile)"
for i in $networks; do
    /usr/sbin/ipset -! add $tmp ${i}
done

# create destination if doesn't exist
/usr/sbin/ipset create -exist $setNetworks $opt &&

# swap temp to dest and delete temp
/usr/sbin/ipset swap $tmp $setNetworks &&
/usr/sbin/ipset destroy $tmp &&
echo "IPSet: $setNetworks updated"
rm -f $tmpFile


if /sbin/iptables -L -n | grep -q -- "$setNetworks";
 then
  :
  # echo "firewall already configured for $setNetworks."
 else
  echo "configuring firewall for $setNetworks."
  /sbin/iptables -A INPUT -m set \! --match-set $setNetworks src -j REJECT
fi 

# check and install on crontab
croncmd="/usr/src/geoblock.sh >> /var/log/geoblock.log 2>&1"
cronjob="0 4 * * * $croncmd"
( crontab -l | grep -v -F "$croncmd" ; echo "$cronjob" ) | crontab -
